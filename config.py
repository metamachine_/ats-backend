import os
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DEBUG = False
    CSRF_ENABLED = True
    WTF_CSRF_SECRET_KEY = '2s5v8y/B?E(H+MbQeShVmYq3t6w9z$C&'
    SECRET_KEY = '2s5v8y/B?E(H+MbQeShVmYq3t6w9z$C&'
    VK_CLIENT_SECRET = 'BcXtWmz1nSmh0voJHb4N'
    # TODO: НА ПРОДЕ ПОМЕНЯТЬ
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:admin@localhost:5432/metamachine'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    FLASK_ADMIN_SWATCH = 'cerulean'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(weeks=7)

class ProductionConfig(Config):
    DEBUG = False

class DevelopConfig(Config):
    DEBUG = True
    ASSETS_DEBUG = True