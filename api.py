import requests
from flask import jsonify, request
from flask_jwt_extended import create_access_token, get_jwt_identity, create_refresh_token, jwt_refresh_token_required, \
    jwt_required
from flask_restful import Resource, abort
from flask_restful.representations import json
from sqlalchemy import desc, func
from werkzeug.exceptions import NotFound
from app import jwt


from models import *
from app import api, app, user_datastore
from schemes import ReportSchema, BusSchema, RouteSchema
from models import Report

from datetime import datetime, timedelta


# bus_schema = BusSchema()
# buses_schema = BusSchema(many=True)
#
# route_schema = RouteSchema()
# routes_schema = RouteSchema(many=True)
#
# report_schema = ReportSchema()
# reports_schema = ReportSchema(many=True)

@jwt.expired_token_loader
def my_expired_token_callback(expired_token):
    token_type = expired_token['type']
    return jsonify({
        'access_token': create_access_token(get_jwt_identity())
    })


class BusMany(Resource):
    def get(self):
        all_buses = Bus.query.all()
        return [{'id':b.id, 'reg_number': b.reg_number} for b in all_buses]


class ReportList(Resource):
    # получаем все жалобы
    def get(self):
        all_reports = Report.query.order_by(Report.date_creation).all()
        return [{'id': r.id, 'bus_id':r.bus_id, 'route_id':r.route_id, 'user_id':r.user_id,
                 'status':r.status, 'negative_score':r.negative_score} for r in all_reports]


class ReportA(Resource):
    # получаем жалобу по id
    def get(self, id):
        return {'data': 'report' + str(id)}

    # изменяем жалобу
    def put(self, id):
        return {'data': 'report is changed'}


class ChangeReport(Resource):
    # отмечаем что жалоба исправленна
    @jwt_required
    def put(self, id):
        user_id = get_jwt_identity()
        r = Report.query.filter_by(user_id=user_id)


        return {'changed_status': 'report ' + str(id)}


class CreateReport(Resource):
    # создаем жалобу
    @jwt_required
    def post(self):
        vk_user_id = get_jwt_identity()
        data = request.get_json()
        print(data)
        bus_id = data['bus']
        route_id = data['route']
        params = data['paramsModel']
        print(params)
        print(vk_user_id)
        params = [1 if value else 0 if not value else value for value in params]
        user_id = User.query.filter_by(email=str(vk_user_id)).first().id
        report = Report(bus_id=bus_id, route_id=route_id, negative_score=sum(params), user_id=user_id)
        db.session.add(report)
        db.session.commit()

        paramsModels = Parameter.query.all()
        for i, p in enumerate(params):
            par_rep = Report_Parameter(report_id=report.id, parameter_id=paramsModels[i].id, value=p)
            db.session.add(par_rep)

        db.session.commit()


class ParametersList(Resource):
    def get(self):
        params = Parameter.query.all()
        return [{'type': x.type, 'name':x.name, 'description': x.description} for x in params]

class RouteA(Resource):
    def get(self):
        routes = Route.query.all()
        return [{'id':r.id, 'number':r.number} for r in routes]

class GraphRoute(Resource):
    def get(self, id):
        result = {}
        reports = Report.query.filter(Report.route_id == id) \
            .filter(Report.date_creation >= (datetime.now() - timedelta(days=30))) \
            .filter(Report.date_creation <= datetime.now()).all()
        if reports.__len__() == 0:
            abort(404)
        for report in reports:
            if report.date_creation in result:
                result[report.date_creation] += 1
            else:
                result[report.date_creation] = 1
        return [{'date': str(key), 'count': value} for (key, value) in result.items()]


class DiagramRoute(Resource):
    def get(self, id):
        result = db.session.execute("select rt.id, p.name, count(p.id) from route rt " + \
                                    "left join  report rp on rt.id =route_id, report__parameter r_p " + \
                                    "left join parameter p on r_p.parameter_id=p.id " + \
                                    "where rt.id=" + str(id) + " and rp.id=r_p.report_id " + \
                                    "group by rt.id, p.name")

        try:
            return [{'name': x.name, 'count': x.count} for x in result]
        except:
            abort(404)



class VKAuth(Resource):
    def get(self):
        code = request.args.get('code')
        redirect_uri = request.args.get('redirect_uri')
        print(redirect_uri)
        print(code)
        response = requests.get('https://oauth.vk.com/access_token', {
            'client_id': 7046497,
            'client_secret': app.config.get('VK_CLIENT_SECRET'),
            'redirect_uri': redirect_uri,
            'code': code
        })

        if 'error' in response:
            return response
        print(response.json())
        vk_user_id = response.json()['user_id']
        if not User.query.filter_by(email=str(vk_user_id)).first():
            user_datastore.create_user(email=str(vk_user_id), password='', roles=['Passenger'])

        db.session.commit()

        return {
            'access_token': create_access_token(identity=vk_user_id),
            'refresh_token': create_refresh_token(identity=vk_user_id)
        }


class TokenRefreshment(Resource):
    @jwt_refresh_token_required
    def get(self):
        vk_user_id = get_jwt_identity()
        return {'access_token': create_access_token(identity=vk_user_id)}


class Summary(Resource):
    def get(self):
        reports = Report.query.filter(Report.date_creation >= (datetime.now() - timedelta(days=30))) \
            .filter(Report.date_creation <= datetime.now()).all()

        if len(reports) == 0:
            abort(404)

        result = {}
        for report in reports:
            if report.route_id in result:
                result[report.route_id] += report.negative_score
            else:
                result.update({report.route_id: report.negative_score})
        return [{'id': key, 'number': Route.query.get(key).number, 'score': value} for (key, value) in result.items()]

class CheckToken(Resource):
    @jwt_required
    def get(self):
        return {'active': True}

class UserReportList(Resource):
    def get(self, id):
        # vk_id = get_jwt_identity()
        user_reports = Report.query.filter_by(user_id=id).all()
        return [{'id': r.id, 'bus_id':r.bus_id, 'route_id':r.route_id, 'user_id':r.user_id,
                 'status':r.status, 'negative_score':r.negative_score} for r in user_reports]

api.add_resource(UserReportList, '/user/reports/<int:id>')
api.add_resource(ReportList, '/report')
# api.add_resource(Report//A, '/report/<int:id>')   # взять конкрет
api.add_resource(CreateReport, '/report') # POST создать репорт
api.add_resource(BusMany, '/buses') # GET получить все автобусы
api.add_resource(RouteA, '/routes') # get все маршруты
api.add_resource(Summary, '/summary')
api.add_resource(VKAuth, '/vk_auth')
api.add_resource(TokenRefreshment, '/refresh_token')
api.add_resource(GraphRoute, '/route/graph/<int:id>') # получить график маршрута
api.add_resource(DiagramRoute, '/route/diagram/<int:id>') # получить диаграмму маршрута
api.add_resource(CheckToken, '/check_token')    # проверить токен на актуальность
api.add_resource(ParametersList, '/params')     # получить все параметры
