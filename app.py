from flask import Flask
from flask_admin import Admin
from flask_jwt_extended import JWTManager
from flask_restful import Api
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_security import SQLAlchemyUserDatastore, Security
from flask_cors import CORS

app = Flask(__name__)
manager = Manager(app)
jwt = JWTManager(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

app.config.from_object('config.DevelopConfig')
db = SQLAlchemy(app)
api = Api(app)
ma = Marshmallow(app)
admin = Admin(app, name='metamachine', template_mode='bootstrap3')

from models import *
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)