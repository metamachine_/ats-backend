# ats-backend

haka-haka-haka-haka-ton

#### Python ver.: =>3.7.2

1. Install requirements:
    `pip install requirements`

2. Change `'SQLALCHEMY_DATABASE_URI'` in `config.py` for connection to db

3. Create migrations folder:
    `python app.py db init`

4. Make first migration and upgrade db:
        
        `python app.py db migrate`
        `python app.py db upgrade`

5. Run server:
    `python app.py runserver -h <host-ip> -p <host-port>`
