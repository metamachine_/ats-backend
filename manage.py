from flask_migrate import Migrate, MigrateCommand

from app import manager, app, db
from models import *
from api import *

migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)
if __name__ == '__main__':
    # create roles
    user_datastore.find_or_create_role(name='Passenger')
    user_datastore.find_or_create_role(name='Moderator')
    db.session.commit()

    manager.run()
