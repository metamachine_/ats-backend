from flask_admin.contrib.sqla import ModelView
from flask_security import RoleMixin, UserMixin
from app import db, admin
from datetime import datetime

# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    vk_user_id = db.Column(db.Integer)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __init__(self, email, password, active, roles, vk_user_id=None):
        self.email = email
        self.password = password
        self.active = active
        self.confirmed_at = datetime.now()
        self.vk_user_id = vk_user_id
        self.roles = roles

    def __str__(self):
        return self.email


class Bus(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    reg_number = db.Column(db.String())
    reports = db.relationship('Report', backref=db.backref('Bus'))

    def __str__(self):
        return self.reg_number


class Route(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    number = db.Column(db.String, nullable=False)
    reports = db.relationship('Report', backref=db.backref('Route'))

    def __str__(self):
        return self.number


class Report(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bus_id = db.Column(db.Integer, db.ForeignKey('bus.id'),
                       nullable=False)
    route_id = db.Column(db.Integer, db.ForeignKey('route.id'),
                         nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    date_creation = db.Column(db.DateTime(), nullable=False, default=datetime.now)
    status = db.Column(db.String)
    negative_score = db.Column(db.Integer, default=0)

    report_parameter = db.relationship('Report_Parameter', backref='Report')

    def __str__(self):
        return 'Bus: %d, Route: %d, Date: %s' % (self.bus_id, self.route_id, \
                                                            self.date_creation.__str__())


class Parameter(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String)
    # 'b' - булево значение
    # 's' - ползунок(slider)
    type = db.Column(db.String, nullable=False)
    description = db.Column(db.String)

    report_parameter = db.relationship('Report_Parameter', backref='Parameter')

    def __str__(self):
        return self.name


class Report_Parameter(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    report_id = db.Column(db.Integer, db.ForeignKey('report.id'),
                          nullable=False)
    parameter_id = db.Column(db.Integer, db.ForeignKey('parameter.id'),
                             nullable=False)
    value = db.Column(db.Float(), nullable=False)

    def __str__(self):
        return self.value.__str__()


admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Report, db.session))
admin.add_view(ModelView(Parameter, db.session))
admin.add_view(ModelView(Report_Parameter, db.session))
admin.add_view(ModelView(Bus, db.session))
admin.add_view(ModelView(Route, db.session))
