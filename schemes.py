from marshmallow import Schema, fields, pprint
from marshmallow.fields import Nested

from models import Bus, Report, Route


class BusSchema(Schema):
    reg_number = fields.String()


class RouteSchema(Schema):
    number = fields.String()


class ReportSchema(Schema):
    date_creation = fields.DateTime()
    checked = fields.Boolean()
    bus_id = fields.Integer()
    bus = fields.Nested(BusSchema)
    route_id = fields.Integer()
    route = Nested(RouteSchema)
